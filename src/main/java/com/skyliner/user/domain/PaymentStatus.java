package com.skyliner.user.domain;

public enum PaymentStatus {

	PENDING,
    PROCESSING,
    COMPLETED,
    FAILED
}
