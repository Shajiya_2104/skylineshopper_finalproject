package com.skyliner.service;

import java.util.List;

import com.skyliner.exception.ProductException;
import com.skyliner.modal.Rating;
import com.skyliner.modal.User;
import com.skyliner.request.RatingRequest;

public interface RatingServices {
	
	public Rating createRating(RatingRequest req,User user) throws ProductException;
	
	public List<Rating> getProductsRating(Long productId);

}
